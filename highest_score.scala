object Scoring {
  
  def score(w: String): Int = {
    val values = ('a' to 'z').zip(1 to 26).toMap
    w.map(values(_)).sum
  }
    
  def high(s: String): String = {
    val words = s.split("\\s")
    val scores = words.map(score(_)).zipWithIndex
    words(scores.maxBy(_._1)._2)
  }
}